using Statistics, Measurements, Random, ADerrors, PyPlot, Base

include("../src/jkvar_types.jl")
include("../src/jkvar_err.jl")
# include("../src/jkvar_math.jl")
# include("../src/jkvar_plot.jl")

for op in (:+, :-, :*, :/, :^)
    @eval function Base.$op(a::jkreal,b::jkreal)
        if a.Njk!=0 && a.Njk==b.Njk
            return jkreal(Base.$op.(a.ensemble,b.ensemble))
        else
            return jkreal(Base.$op(a.meas,b.meas))
        end
    end

    @eval function Base.$op(a::jkreal,b::Number)
        if a.Njk!=0
            return jkreal(Base.$op.(a.ensemble,b))
        else
            return jkreal(Base.$op(a.meas,b))
        end
    end

    @eval function Base.$op(a::Number,b::jkreal)
        if a.Njk!=0
            return jkreal(Base.$op.(a,b.ensemble))
        else
            return jkreal(Base.$op(a,b.meas))
        end
    end


end

##

using jkvar, Measurements


a = jkreal(measurement("0.1533(10)"))

1/a