for op in (:sin, :cos, :tan, :log, :exp, :sqrt, :sind, :cosd, :tand, :sinpi, :cospi, :sinh, :cosh, :tanh, :asin, :acos, :atan, :asind, :acosd, :atand, :sec, :csc, :cot, :secd, :cscd, :cotd, :asec, :acsc, :acot, :asecd, :acscd, :acotd, :sech, :csch, :coth, :asinh, :acosh, :atanh, :asech, :acsch, :acoth, :sinc, :cosc, :deg2rad, :rad2deg, :log2, :log10, :log1p, :exp2, :exp10, :expm1, :-)
    @eval function Base.$op(a::jkreal)
        if a.Njk==0
            return jkreal(Base.$op(a.meas))
        else
            return jkreal(Base.$op.(a.ensemble))
        end
    end
end 

for op in (:+, :-, :*, :/, :^)
    @eval function Base.$op(a::jkreal,b::jkreal)
        if a.Njk!=0 && a.Njk==b.Njk
            return jkreal(Base.$op.(a.ensemble,b.ensemble))
        else
            return jkreal(Base.$op(a.meas,b.meas))
        end
    end
    
    @eval function Base.$op(a::jkreal,b::Number)
        if a.Njk!=0
            return jkreal(Base.$op.(a.ensemble,b))
        else
            return jkreal(Base.$op(a.meas,b))
        end
    end

    @eval function Base.$op(a::Number,b::jkreal)
        if b.Njk!=0
            return jkreal(Base.$op.(a,b.ensemble))
        else
            return jkreal(Base.$op(a,b.meas))
        end
    end




end

for op in (:<, :>, :≤, :≥, :≠)
    @eval Base.$op(a::jkreal, b::jkreal) = Base.$op(a.val, b.val)
    @eval Base.$op(a::jkreal, b::Number) = Base.$op(a.val, b)
    @eval Base.$op(a::Number, b::jkreal) = Base.$op(a, b.val)
end

##

####################################### VECTORIZED OPERATIONS ##########################################
# for op in (:sin, :cos, :tan, :log, :exp, :sqrt, :sinh, :cosh, :-)
#     @eval function Base.$(Symbol(op,"."))(a::Vector{jkreal})
#         aux = Vector{jkreal}(undef,length(a))
#         @simd for i in eachindex(aux)
#             @inbounds aux[i] = Base.$op(a)
#         end
#         return aux
#     end
# end 

# for op in (:+, :-, :*, :/, :^)
#     @eval function Base.$(Symbol(".",op))(a::Vector{jkreal},b::Vector{jkreal})
#         length(a)!=length(b) ? error("SizeError: vectors do not have the same size") : 0
#         aux = Vector{jkreal}(undef,length(a))
#         @simd for i in eachindex(aux)
#             @inbounds aux[i] = Base.$op(a[i],b[i])
#         end
#         return aux
#     end
#     @eval function Base.$(Symbol(".",op))(a::Vector{jkreal},b::jkreal)
#         aux = Vector{jkreal}(undef,length(a))
#         @simd for i in eachindex(aux)
#             @inbounds aux[i] = Base.$op(a[i],b)
#         end
#         return aux
#     end
#     @eval Base.$op(b::jkreal,a::Vector{jkreal}) = Base.$op(a,b)

#     @eval function Base.$(Symbol(".",op))(a::Vector{jkreal},b::Vector{T}) where T<:Number
#         length(a)!=length(b) ? error("SizeError: vectors do not have the same size") : 0
#         aux = Vector{jkreal}(undef,length(a))
#         @simd for i in eachindex(aux)
#             @inbounds aux[i] = Base.$op(a[i],b[i])
#         end
#         return aux
#     end
#     @eval Base.$op(b::Vector{T},a::Vector{jkreal}) where T<:Number = Base.$op(a,b)
#     @eval function Base.$(Symbol(".",op))(a::Vector{jkreal},b::T) where T<:Number
#         aux = Vector{jkreal}(undef,length(a))
#         @simd for i in eachindex(aux)
#             @inbounds aux[i] = Base.$op(a[i],b)
#         end
#         return aux
#     end
#     @eval Base.$op(b::T,a::Vector{jkreal}) where T<:Number = Base.$(op_symbols[i])(a,b)
# end


# ### ========================= ^ ==============================
# function Base.:^(a::Vector{jkreal},b::jkreal)
#     aux = Vector{jkreal}(undef,length(a))
#     @simd for i in eachindex(aux)
#         @inbounds aux[i] = Base.^(a[i],b)
#     end
#     return aux
# end
# function Base.:^(a::Vector{jkreal},b::T) where T<:Number
#     aux = Vector{jkreal}(undef,length(a))
#     @simd for i in eachindex(aux)
#         @inbounds aux[i] = Base.^(a[i],b)
#     end
#     return aux
# end
# function Base.:^(a::Vector{T},b::jkreal) where T<:Number
#     aux = Vector{jkreal}(undef,length(a))
#     @simd for i in eachindex(aux)
#         @inbounds aux[i] = Base.^(a[i],b)
#     end
#     return aux
# end
# ### =================================================================






