function scatterplot(xx::Vector{T},yy::Vector{jkreal};kwargs...) where T<:Number
    pycall(plt.errorbar,PyAny,xx,val.(yy),yerr=err.(yy);kwargs...)
    return 0
end
function scatterplot(xx::Vector{jkreal},yy::Vector{T};kwargs...) where T<:Number
    pycall(plt.errorbar,PyAny,val.(xx),yy,xerr=err.(xx);kwargs...)
    return 0
end
function scatterplot(xx::Vector{jkreal},yy::Vector{jkreal};kwargs...)
    pycall(plt.errorbar,PyAny,val.(xx),val.(yy),xerr=err.(xx),yerr=err.(yy);kwargs...)
    return 0
end


function behold()
    grid()
    legend()
    display(gcf())
    close()
    return 0
end
function behold(saveto::String)
    grid()
    legend()
    savefig(saveto)
    close()
    return 0
end


function latex(;size=10)
    rcParams = PyPlot.PyDict(PyPlot.matplotlib."rcParams")
    rcParams["text.usetex"] = true
    rcParams["mathtext.fontset"]  = "cm"
    rcParams["font.size"] = size
    rcParams["axes.labelsize"] = size
    plt.rc("text", usetex=true)
end
