"""
    Last modification: July 13th, 2022

    This is a `julia` library to handle error propagation with jackknife error.
"""
module jkvar

    using Statistics, Measurements, Random, PyCall, PyPlot, Base

    include("jkvar_types.jl")
    include("jkvar_math.jl")
    include("jkvar_err.jl")
    include("jkvar_plot.jl")

    export jkreal, val, err
    export jkk, rebin
    export scatterplot, behold, latex

end 