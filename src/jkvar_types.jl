"""
    jkreal(meas::Measurement{Float64})
    jkreal(val::Float64,err::Float64)
    jkreal(vector::Vector{T}; unbias=false) where T<:Number
    jkreal(vector::Vector{T}, δrange::UnitRange{Int64}; unbias=false) where T<:Number
    
### ATTRIBUTES
    meas::Measurement{Float64}
    ensemble::Vector{Float64}
    Njk::Int64

Returns a `jkreal` composite type.

## Constructors' arguments
    - `Measurement{Float64}`     : returns a `Measurement` variable, but no ensemble variable
    - Two `Float64`              : returns `measurement(first,second)`
    - `Vector{T<:Number}`        : `ensemble` attribute is filled with vector and `mean(vector)` returned for the mean value and `jkk(vector)`
    - `δrange::UnitRange{Int64}`: same as `jkreal(vector)` but it is first rebinned with binsize ∈ δrange, than `jkk` is called for each binsize. Then the error is averaged
"""
mutable struct jkreal
    ensemble::Vector{Float64}
    meas::Measurement{Float64}
    Njk::Int64

    jkreal(x::jkreal) = new(x.ensemble,x.meas,x.Njk)
    jkreal(meas::Measurement{Float64}) = new([],meas,0)
    jkreal(val::T,err::T) where T<:Number = new([],measurement(val,err),0)
    function jkreal(vector::Vector{T}; unbias=false) where T<:Number
        this = new()
        this.ensemble = vector
        this.Njk = length(vector)
    
        meanval = mean(vector);  unbias ? val = meanval - (this.Njk-1)*(mean(vector)-meanval) : val = meanval
        err = jkk(vector)
        this.meas = measurement(val,err)
    
        return this
    end
    function jkreal(vector::Vector{T},δrange::UnitRange{Int64}; unbias=false) where T<:Number
        this = new()
        this.ensemble = vector
        this.Njk = length(vector)

        check = [i for i in δrange if i>length(vector)]
        if length(check)>0 error("binsize range exceed length of ensemble") end

        meanval = mean(vector);  unbias ? val = meanval - (this.Njk-1)*(mean(vector)-meanval) : val = meanval
        aux = []
        for width in δrange
            vec_binned = rebin(vector,width)
            push!(aux,jkk(vec_binned))
        end
        error = mean(aux)
        this.meas = measurement(val,error)

        return this
    end
end

val(x::jkreal) = x.meas.val
err(x::jkreal) = x.meas.err



function Base.show(io::IO, a::jkreal)
    print(io,"$(a.meas.val) ± $(a.meas.err)")
    return
end

