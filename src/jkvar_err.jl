function jkk(vector::Vector{T})::T where T<:Number
    N = length(vector)
    aux = Vector{T}(undef,N)
    for jk in 1:N-1
        aux[jk] = mean(vector[vcat(1:jk-1,jk+1:end)])
    end
    aux[N] = mean(vector[1:end-1])
    error = sqrt((N-1)/N*sum( (aux .- mean(aux)).^2 ))
    return convert(T,error)
end

function rebin(V::Vector{T},width::Int64) where T<:Number
    L = length(V)
    if width>L error("Binsize $width is bigger than vector size.") end
    if width==1 return V end

    aux = vcat(
        vec(mean(reshape(V[1:L÷width*width],(width,L÷width)),dims=1)),
        (L%width==0 ? [] : mean(V[end+1-L%width:end]))
    )   

    return convert.(T,aux)
end
